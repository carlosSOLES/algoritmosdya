package grafos;

public class Arista {

	String nombre;
	String id;
	float len;
	int peso;
	String nodoA;
	String nodoB;

	// Constructor 0, vacío
	Arista() {
	}

	// Constructor 1, dar un nodo a conectar
	Arista(String b) {
		nodoB = b;
	}

	// Constructor 2, dar los dos aristas
	Arista(String a, String b) {
		nodoA = a;
		nodoB = b;
	}
}
