package grafos;

import java.lang.String;

public class Nodo {
	
	// Atributos de la clase
	String nombre;
	int id;
	double x;
	double y;
	
	// Constructor 0, vacío
	Nodo(){}
	
	//Constructor 1, recibe un id numerico, su nombre será su id en String
	Nodo(int id_num){
		id = id_num;
		nombre = Integer.toString(id_num);
	}
	
	// Constructor 2, recibe un id numerico y un nombre
	Nodo(int id_num, String id_str){
		id = id_num;
		nombre = id_str;
	}
	
	// En caso de necesitar inicializar cooordenadas manualmente
	void setCoord(double xx, double yy){
		x = xx;
		y = yy;
	}
	
	// Inicializar coordenadas aleatoriamente
	void setCoordRand() {
		x = Math.random();
		y = Math.random();
	}
	
	// Devuelve la distancia de este nodo a otro
	double distancia(Nodo n) {
		return Math.sqrt(Math.pow((x - n.x), 2) + Math.pow((y - n.y), 2));
	}
}
