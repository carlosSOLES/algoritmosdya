package grafos;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.Random;
import java.util.Map;
import java.io.FileOutputStream;

public class Grafo {

	// Características base para un Grafo

	boolean dir = false; // No dirigido por defecto
	// Maps para nodos y aristas
	HashMap<String, Nodo> nodos = new HashMap<String, Nodo>();
	HashMap<String, ArrayList<Arista>> aristas = new HashMap<String, ArrayList<Arista>>();

	// Generan el archivo dot
	StringBuilder builder = new StringBuilder();
	String dotStr;

	void toDot() {

		if (dir) {
			builder.append("digraph G{");
		} else {
			builder.append("graph G{");
		}
		for (Map.Entry me : nodos.entrySet()) {
			Nodo nn = new Nodo();
			nn = nodos.get(me.getKey());
			System.out.println("Value: " + nn.id);
			builder.append(nn.id);
			builder.append(";");
		}

		int tt = 0;
		for (Map.Entry me : aristas.entrySet()) {
			ArrayList<Arista> aa = new ArrayList<Arista>();
			aa = aristas.get(me.getKey());
			// System.out.println("Nodo:" + nodos.get(me.getKey()).nombre);
			for (Arista a : aa) {
				System.out.println("(" + me.getKey() + "," + a.nodoB + ");");

				if (dir) {
					builder.append(me.getKey() + "->" + a.nodoB + ";");
				} else {
					builder.append(me.getKey() + "--" + a.nodoB + ";");
				}
				tt++;
			}

		}
		builder.append("}");
		dotStr = builder.toString();
		System.out.println(dotStr);
		System.out.println("Total de aristas:" + Integer.toString(tt));
		try {
			FileOutputStream outputStream = new FileOutputStream("test.dot");
			byte[] strToBytes = dotStr.getBytes();
			outputStream.write(strToBytes);
			outputStream.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	void toDot(String fileName) {
		builder.append("graph G{");
		for (Map.Entry me : nodos.entrySet()) {
			Nodo nn = new Nodo();
			nn = nodos.get(me.getKey());
			System.out.println("Value: " + nn.id);
			builder.append(nn.id);
			builder.append(";");
		}

		int tt = 0;
		for (Map.Entry me : aristas.entrySet()) {
			ArrayList<Arista> aa = new ArrayList<Arista>();
			aa = aristas.get(me.getKey());
			System.out.println("Nodo:" + nodos.get(me.getKey()).nombre);
			for (Arista a : aa) {
				// System.out.println("(" + me.getKey() + "," + a.nodoB + ");");
				builder.append(me.getKey() + "--" + a.nodoB + ";");
				tt = tt + 1;
			}

		}

		builder.append("}");
		dotStr = builder.toString();
		System.out.println(dotStr);
		System.out.println("Total de aristas:" + Integer.toString(tt));
		try {
			FileOutputStream outputStream = new FileOutputStream(fileName);
			byte[] strToBytes = dotStr.getBytes();
			outputStream.write(strToBytes);
			outputStream.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	// Constructor base aleatorio entre 1 y 10 nodos y entre 1 y 15 aristas
	public static Grafo genErdosRenyi() {
		Grafo g = new Grafo();
		Random rand = new Random();

		// Creacion aleatoria de m y n
		int n = rand.nextInt(10) + 1;
		int m = rand.nextInt(15) + 1;

		// Crear nodos base y guardarlos en el map
		for (int i = 0; i <= n; i++) {

			String iStr = Integer.toString(i);

			g.nodos.put(iStr, new Nodo(i));
			ArrayList<Arista> aristas_vacia = new ArrayList<Arista>();
			g.aristas.put(iStr, aristas_vacia);
		}

		// Crear m aristas
		for (int i = 0; i <= m; i++) {
			int na = rand.nextInt(n);
			int nb = rand.nextInt(n);

			String naStr = Integer.toString(na);
			String nbStr = Integer.toString(nb);

			g.aristas.get(naStr).add(new Arista(nbStr));
			System.out.println(g.aristas.get(naStr));

		}
		return g;
	}

	// Constructor con parametros para nodos, aristas
	public static Grafo genErdosRenyi(int nn, int mm) {
		Grafo g = new Grafo();
		Random rand = new Random();

		// Asignar los valores de los parámetros
		int n = nn;
		int m = mm;

		// Crear nodos base y guardarlos en el map
		for (int i = 0; i <= n; i++) {

			String iStr = Integer.toString(i);
			g.nodos.put(iStr, new Nodo(i));
			ArrayList<Arista> aristas_vacia = new ArrayList<Arista>();
			g.aristas.put(iStr, aristas_vacia);
		}

		// Crear m aristas
		for (int i = 0; i <= m; i++) {
			int na = rand.nextInt(n);
			int nb = rand.nextInt(n);

			String naStr = Integer.toString(na);
			String nbStr = Integer.toString(nb);

			g.aristas.get(naStr).add(new Arista(nbStr));
			System.out.println(g.aristas.get(naStr));

		}
		return g;
	}

	public static Grafo genErdosRenyi(int nn, int mm, boolean dirigido, boolean auto) {
		Grafo g = new Grafo();
		Random rand = new Random();

		// Asignar los valores de los parámetros
		int n = nn;
		int m = mm;
		g.dir = dirigido;

		// Crear nodos base y guardarlos en el map
		for (int i = 0; i <= n; i++) {

			String iStr = Integer.toString(i);
			g.nodos.put(iStr, new Nodo(i));
			ArrayList<Arista> aristas_vacia = new ArrayList<Arista>();
			g.aristas.put(iStr, aristas_vacia);
		}

		// Crear m aristas
		for (int i = 0; i <= m - 1; i++) {
			int na;
			int nb;

			// Elegir dos aristas a conectar aleatoriamente
			// En caso de no permitir autociclos repite hasta tener diferentes
			if (auto) {
				na = rand.nextInt(n);
				nb = rand.nextInt(n);
			} else {
				do {
					na = rand.nextInt(n);
					nb = rand.nextInt(n);
				} while (na == nb);
			}

			String naStr = Integer.toString(na);
			String nbStr = Integer.toString(nb);

			g.aristas.get(naStr).add(new Arista(nbStr));
			System.out.println(g.aristas.get(naStr));

		}
		return g;
	}

	public static Grafo genGilbert(int n, double p, boolean dirigido, boolean auto) {
		Grafo g = new Grafo();
		Random rand = new Random();
		g.dir = dirigido;

		// Crear nodos base y guardarlos en el map
		for (int i = 0; i <= n; i++) {

			String iStr = Integer.toString(i);
			g.nodos.put(iStr, new Nodo(i));
			ArrayList<Arista> aristas_vacia = new ArrayList<Arista>();
			g.aristas.put(iStr, aristas_vacia);
		}

		// Ciclos para la creacion de los aristas
		for (int i = 0; i <= n; i++) {
			int aux;
			int na = i;
			int nb;

			String naStr = Integer.toString(na);

			// En caso de autociclo no permitido comienza en el siguiente
			if (auto)
				aux = 0;
			else
				aux = 1;

			for (int j = i + aux; j <= n; j++) {
				if (Math.random() < p) {
					nb = j;
					String nbStr = Integer.toString(nb);
					g.aristas.get(naStr).add(new Arista(nbStr));
				}
			}
		}

		return g;
	}

	public static Grafo genGeografico(int n, double r, boolean dirigido, boolean auto) {
		Grafo g = new Grafo();
		Random rand = new Random();
		g.dir = dirigido;

		// Crear nodos base, asignarles una coordenada aleatoria y guardarlos en el map
		for (int i = 0; i <= n; i++) {

			String iStr = Integer.toString(i);
			Nodo n_aux = new Nodo(i);
			n_aux.setCoordRand();
			g.nodos.put(iStr, n_aux);
			ArrayList<Arista> aristas_vacia = new ArrayList<Arista>();
			g.aristas.put(iStr, aristas_vacia);
		}

		// Crear aristas
		for (int i = 0; i <= n; i++) {
			int na = i;
			int nb;
			int aux;
			double dist;

			String naStr = Integer.toString(na);
			Nodo ni = g.nodos.get(naStr);

			// En caso de autociclo no permitido comienza en el siguiente
			if (auto)
				aux = 0;
			else
				aux = 1;
			
			// Revisar distancia a los dem�s nodos
			for (int j = i + aux; j <= n; j++) {
				nb = j;
				String nbStr = Integer.toString(nb);
				Nodo nj = g.nodos.get(nbStr);
				//System.out.println(naStr + "-" + nbStr);
				dist = ni.distancia(nj); // Mide la distancia del nod i al j

				if (dist <= r) {
					g.aristas.get(naStr).add(new Arista(nbStr)); // A�ade el nodo
				}
			}

		}

		return g;
	}

	public static Grafo genBarabasiAlbert(int n, double d, boolean dirigido, boolean auto) {
		Grafo g = new Grafo();
		Random rand = new Random();
		g.dir = dirigido;

		// Primer nodo
		g.nodos.put("0", new Nodo(0));
		ArrayList<Arista> aris0 = new ArrayList<Arista>();
		g.aristas.put("0", aris0);

		// Crear nodos uno por uno
		for (int i = 1; i <= n-1; i++) {
			
			// Nodo actual
			String iStr = Integer.toString(i);
			g.nodos.put(iStr, new Nodo(i));
			ArrayList<Arista> aristas_vacia = new ArrayList<Arista>();
			g.aristas.put(iStr, aristas_vacia);
			
			// Conectar con nodos previos
			for (int j = 0; j < i; j++) {
				String jStr = Integer.toString(j);
				int dg = g.aristas.get(jStr).size(); // Grado del nodo
				System.out.println("Grado nodo" + jStr + ": " + Integer.toString(dg));
				double p = 1 - dg / d; // Probabilidad de unir
				if (Math.random() < p) {
					g.aristas.get(iStr).add(new Arista(jStr));
					g.aristas.get(jStr).add(new Arista(iStr));
				}
			}
		}

		return g;
	}

}
